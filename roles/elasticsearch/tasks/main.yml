---
#
# Installing Elasticsearch
#

# Install Java

- name: Install Java with dnf
  dnf:
   name: java-1.8.0-openjdk.x86_64
   update_cache: yes

- name: Install firewalld with dnf
  dnf:
   name: firewalld
   update_cache: yes

# Create Elasticsearch Repository & Install Elasticsearch

- name: Import Elasticsearch GPG Key
  rpm_key:
    state: present
    key: https://artifacts.elastic.co/GPG-KEY-elasticsearch

- name: Install Elastic RPM Repository
  copy:
    src: elasticsearch.repo
    dest: /etc/yum.repos.d/elasticsearch.repo

- name: Install Elasticsearch with dnf
  dnf:
   name: elasticsearch
   update_cache: yes

# Configurations

- name: Pause for 0.5 minutes
  pause:
    minutes: 0.166667

- name: Backup Config
  raw: cp /etc/elasticsearch/elasticsearch.yml /etc/elasticsearch/elasticsearch.yml.bak
  #ansible.builtin.copy:
  #src: /etc/elasticsearch/elasticsearch.yml
  #dest: /etc/elasticsearch/elasticsearch.yml.bak

## Cluster

- name: Defining Cluster Name
  lineinfile:
   destfile: /etc/elasticsearch/elasticsearch.yml
   regexp: 'cluster.name'
   line: 'cluster.name: {{ cluster_name }}'

## Node

- name: Defining Node Name
  lineinfile:
   destfile: /etc/elasticsearch/elasticsearch.yml
   regexp: 'node.name'
   line: 'node.name: {{ node_name }}'

## Memory

- name: Defining Bootstrap Memory
  lineinfile:
   destfile: /etc/elasticsearch/elasticsearch.yml
   regexp: 'bootstrap.memory_lock:'
   line: 'bootstrap.memory_lock: true'

## Network

- name: Defining Server IP
  lineinfile:
   destfile: /etc/elasticsearch/elasticsearch.yml
   regexp: 'network.host:'
   line: 'network.host: {{ server_ip }}'

- name: Defining HTTP Port
  lineinfile:
   destfile: /etc/elasticsearch/elasticsearch.yml
   regexp: 'http.port:'
   line: 'http.port: 9200'

## Discovery

- name: Defining Discovery Ip
  lineinfile:
    destfile: /etc/elasticsearch/elasticsearch.yml
    regexp: 'discovery.seed_hosts:'
    line: 'discovery.seed_hosts: [ "{{ discovery_hosts1 }}", "{{ discovery_hosts2 }}" ]'

    #["{{ discovery_hosts1 }}", "{{ discovery_hosts2 }}", "{{ discovery_hosts3 }}"]

- name:  Defining Cluster Master Eligible Nodes
  lineinfile:
    destfile: /etc/elasticsearch/elasticsearch.yml
    regexp: 'cluster.initial_master_nodes:'
    line: 'cluster.initial_master_nodes: [ "{{ cluster_master_nodes1 }}", "{{ cluster_master_nodes2 }}" ]'

    #["{{ cluster_master_nodes1 }}", "{{ cluster_master_nodes2 }}", "{{ cluster_master_nodes3 }}" ]

# Set Java Opts

- name:  Additional Java OPTS
  lineinfile:
    destfile: /etc/sysconfig/elasticsearch
    regexp: 'ES_JAVA_OPTS='
    line: "ES_JAVA_OPTS='-Xmx{{ java_opts }} -Xms{{ java_opts }}'"

- name: Max Locked Up Memory
  lineinfile:
    destfile: /etc/sysconfig/elasticsearch
    regexp: 'MAX_LOCKED_MEMORY=unlimited'
    line: 'MAX_LOCKED_MEMORY=unlimited'

# Systemd Service Configuration

- name: Create a Systemd Service Directory
  ansible.builtin.file:
    path: /etc/systemd/system/elasticsearch.service.d
    state: directory
    mode: '0755'

- name: Systemd Service Configuration
  blockinfile:
    path: /etc/systemd/system/elasticsearch.service.d/override.conf
    create: yes
    mode: u+rw,g-wx,o-rwx
    block: |
      [Service]
      LimitMEMLOCK=infinity

# Optimize Elasticsearch Security Limits

- name: Add Hard Memlock
  community.general.pam_limits:
    domain: elasticsearch
    limit_type: hard
    limit_item: memlock
    value: unlimited
    comment: unlimited hard memory lock for elasticsearch

- name: Add Soft Nofile
  community.general.pam_limits:
    domain: elasticsearch
    limit_type: soft
    limit_item: nofile
    value: 65536

- name: Add Hard Nofile
  community.general.pam_limits:
    domain: elasticsearch
    limit_type: hard
    limit_item: nofile
    value: 65536

# Starting Firewalld

- name: Force Systemd to Reread Configs
  ansible.builtin.systemd:
    daemon_reload: yes

- name: Starting & Enable firewalld
  service:
   enabled: yes
   name: firewalld
   state: started

# Enable Port at Firewalld

- name: Enable Port at Firewalld
  ansible.posix.firewalld:
    port: 9200/tcp
    permanent: yes
    state: enabled

- name: Enable Port at Firewalld
  ansible.posix.firewalld:
    port: 9300/tcp
    permanent: yes
    state: enabled

- name: Reload Firewalld
  command: firewall-cmd --reload

# Starting Elasticsearch

- name: Force Systemd to Reread Configs
  ansible.builtin.systemd:
    daemon_reload: yes

- name: Starting & Enable Elasticsearch
  service:
   enabled: yes
   name: elasticsearch
   state: started
